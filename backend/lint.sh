poetry run black .
poetry run isort .
poetry run ruff --fix .
poetry run flake8 .