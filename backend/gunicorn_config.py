bind = "0.0.0.0:5000"
workers = 4  # Number of worker processes
capture_output = True  # Capture stdout/stderr
loglevel = "debug"  # Debug logging level
accesslog = "-"  # Access log to stdout
errorlog = "-"  # Error log to stdout
