import timeit
from datetime import datetime
from functools import wraps

from flask import request

from app import ENV
from app.events.enums import EventType, QueueName
from app.events.message_redis import publish_to_queue
from app.featureflags import get_current_features


def route_timing(f, request, elapsed_time):
    publish_to_queue(
        {
            "env": ENV,
            "event_type": EventType.ROUTE_TIMING.value,
            "user_id": None,
            "queue_name": QueueName.STATS.value,
            "data": {
                "time": elapsed_time,
                "uri": request.path,
                "name": f.__name__,
                "timestamp": str(datetime.now().isoformat()),
                "features": get_current_features(),
            },
        },
        queue_name=f"{ENV}_{QueueName.STATS.value}",
        event_type=EventType.ROUTE_TIMING.value,
    )


def caching_timing(f, elapsed_time):
    publish_to_queue(
        {
            "env": ENV,
            "event_type": EventType.CACHING_TIMING.value,
            "user_id": None,
            "queue_name": QueueName.STATS.value,
            "data": {
                "time": elapsed_time,
                "uri": request.path,
                "name": f.__name__,
                "timestamp": str(datetime.now().isoformat()),
                "features": get_current_features(),
            },
        },
        queue_name=f"{ENV}_{QueueName.STATS.value}",
        event_type=EventType.ROUTE_TIMING.value,
    )


def timeit_decorator_factory(logger=None, action=route_timing):
    def timeit_decorator(f):
        @wraps(f)
        def wrapper(*args, **kwargs):
            start_time = timeit.default_timer()
            result = f(*args, **kwargs)
            elapsed_time = timeit.default_timer() - start_time
            if logger:
                logger.info(f"{f.__name__} took {elapsed_time} seconds to execute")
            action(f, request, elapsed_time)
            return result

        return wrapper

    return timeit_decorator
