from enum import Enum
from typing import TypedDict, Union


class EventType(Enum):
    BACKUP = "backup"
    EMAIL_BACKUP = "email_backup"
    # BOOKING
    ADD_BOOKING = "add_booking"
    EDIT_BOOKING = "edit_booking"
    DELETE_BOOKING = "delete_booking"
    CACHE_LATEST_BOOKINGS = "cache_latest_bookings"
    # CAR
    ADD_CAR = "add_car"
    DELETE_CAR = "delete_car"
    EDIT_CAR = "edit_car"
    # USER
    REGISTER_USER = "register_user"
    LOGIN_USER = "login_user"
    ADD_USER = "add_user"
    EDIT_USER = "edit_user"
    DELETE_USER = "delete_user"
    # STAFF
    ADD_STAFF = "add_staff"
    EDIT_STAFF = "edit_staff"
    DELETE_STAFF = "delete_staff"
    # PAYMENT
    ADD_PAYMENT = "add_payment"
    EDIT_PAYMENT = "edit_payment"
    DELETE_PAYMENT = "delete_payment"
    # STATS
    ROUTE_TIMING = "route_timing"
    CACHING_TIMING = "caching_timing"
    CACHE_LATEST_RESOURCE_TIMELINE = "cache_latest_resource_timeline"


class QueueName(Enum):
    BOOKING = "booking"
    CAR = "car"
    USER = "user"
    STAFF = "staff"
    PAYMENT = "payment"
    BACKUP = "backup"
    STATS = "stats"
    CACHE = "cache"


class MessageType(TypedDict):
    env: str
    event_type: Union[
        str, EventType
    ]  # Allows both plain strings and EventType enum values
    user_id: int
    queue_name: Union[str, QueueName]


class CacheOperation(Enum):
    BOOKING_LIST = "booking_list"
    CAR_LIST = "car_list"
    RESOURCE_USAGE = "resource_usage"
