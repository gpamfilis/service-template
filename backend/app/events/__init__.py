import logging

from app.events.caching import cache_list_bookings_msg, cache_resource_usage_msg
from app.events.enums import CacheOperation

logger = logging.getLogger(__name__)

CACHING = {
    CacheOperation.BOOKING_LIST: cache_list_bookings_msg,
    CacheOperation.RESOURCE_USAGE: cache_resource_usage_msg,
}


def cache_user_routes(cache, user_id=1):
    """
    Caches user routes in Redis.

    :param cache: The Redis cache to use for caching.
    :type cache: redis.Redis
    :param user_id: The ID of the user whose routes to cache. Defaults to 1.
    :type user_id: int
    """
    logger.info("Caching user routes")
    for cache_name in cache:
        logger.info(f"Caching {cache_name}")
        # Ensure the cache_name is an instance of CacheOperation
        if not isinstance(cache_name, CacheOperation):
            logger.error(f"Invalid cache operation: {cache_name}")
            continue

        cache_function = CACHING.get(cache_name)
        if cache_function:
            cache_function(user_id=user_id)
        else:
            logger.debug(f"No caching function found for '{cache_name.name}'")
