import json
import logging
import os
import time
import traceback
from hashlib import sha256

import redis

from app import ENV
from app.notification.email import send_email_error

logger = logging.getLogger(__name__)


def hash_message(message: str) -> str:
    return sha256(message.encode()).hexdigest()


def create_redis_client(url, max_retries=3, retry_delay=3, timeout=5):
    """Create a Redis client with retry logic."""
    retry_count = 0
    while retry_count < max_retries:
        try:
            logger.info(f"Attempt {retry_count + 1}: Connecting to Redis...")
            client = redis.Redis.from_url(url, socket_connect_timeout=timeout)
            client.ping()
            logger.info("Redis connection successful.")
            return client
        except redis.ConnectionError as conn_err:
            logger.warning(
                f"Redis connection error: {conn_err}. Retrying in {retry_delay} seconds..."
            )
            time.sleep(retry_delay)  # Moved inside the except block
        except Exception as e:
            logger.warning(
                f"Unexpected error: {e}. Retrying in {retry_delay} seconds..."
            )
            time.sleep(retry_delay)  # Moved inside the except block
        finally:
            retry_count += 1

    error_message = f"Unable to connect to Redis after {max_retries} attempts."
    logger.error(error_message)
    send_email_error(f"{ENV} Redis Connection Error", error_message)
    raise redis.ConnectionError("Unable to connect to Redis after maximum retries.")


def publish_to_queue(message, queue_name="my-queue", event_type="test"):
    """
    Publish a message to a specified Redis queue and send an email if it fails.

    Parameters:
    - message (dict or str): The message to publish. If it's a dictionary, it will be converted to a JSON string.
    - queue_name (str): The name of the Redis queue to which the message will be published.
    """
    logger.info(
        f"Calling publish_to_queue: queue_name: {queue_name},  event_type: {event_type}"
    )
    try:
        logger.info("Creating Redis client")
        # Initialize Redis client
        redis_url = os.environ.get("REDIS_URL")
        r = create_redis_client(redis_url)

        # Convert message to JSON string if it's a dictionary
        if isinstance(message, dict):
            message = json.dumps(message)

        # Publish the message to the specified Redis queue
        r.rpush(queue_name, message)
        logger.info(f"Message added to queue {queue_name}")

    except Exception:
        traceback_info = traceback.format_exc()

        logger.error(f"An error occurred while publishing to queue: {traceback_info}")
        send_email_error(
            subject=f"{ENV}_Failed Message Queue Notification",
            message_data=traceback_info,
        )  # Send email notification


def add_message_to_queue(message, check_duplicates: bool = True):
    try:
        redis_url = os.environ.get("REDIS_URL")
        r = create_redis_client(redis_url)

        message_str = json.dumps(
            message, sort_keys=True
        )  # Sort keys for consistent hashing

        queue_name = message["queue_name"]
        env = message["env"]

        if not check_duplicates:
            r.rpush(f"{env}_{queue_name}", message_str)
            r.close()
            return

        message_hash = hash_message(message_str)

        existing_messages_str = r.get(f"{env}_{queue_name}_messageJSON")
        existing_messages = (
            json.loads(existing_messages_str) if existing_messages_str else {}
        )

        if message_hash not in existing_messages:
            # Add message to the list-based queue
            r.rpush(f"{env}_{queue_name}", message_str)

            # Mark the message as seen in JSON
            existing_messages[message_hash] = True
            r.set(f"{env}_{queue_name}_messageJSON", json.dumps(existing_messages))
            r.close()
            print(f"Added to queue: {message}")
            return
        else:
            print(f"Duplicate message: {message}")
            return

    except redis.RedisError as redis_err:
        print(f"Redis Error: {redis_err}")
        return
    except json.JSONDecodeError as json_err:
        print(f"JSON Decode Error: {json_err}")
        return
    except Exception as e:
        print(f"An unexpected error occurred: {e}")
        return
