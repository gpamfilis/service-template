from app import ENV
from app.events.enums import EventType, QueueName
from app.events.message_redis import publish_to_queue


def cache_list_bookings_msg(user_id=1):
    publish_to_queue(
        message={
            "env": ENV,
            "event_type": EventType.CACHE_LATEST_BOOKINGS.value,
            "queue_name": f"{ENV}_{QueueName.CACHE.value}",
            "data": {"user_id": user_id},
        },
        queue_name=f"{ENV}_{QueueName.CACHE.value}",
        event_type=EventType.CACHE_LATEST_BOOKINGS.value,
    )


def cache_resource_usage_msg(user_id=1):
    publish_to_queue(
        message={
            "env": ENV,
            "event_type": EventType.CACHE_LATEST_RESOURCE_TIMELINE.value,
            "queue_name": f"{ENV}_{QueueName.CACHE.value}",
            "data": {"user_id": user_id},
        },
        queue_name=f"{ENV}_{QueueName.CACHE.value}",
        event_type=EventType.CACHE_LATEST_RESOURCE_TIMELINE.value,
    )
