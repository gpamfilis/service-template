import logging
import logging.config as logconf
import os

from dotenv import load_dotenv
from flask import Flask
from flask_cors import CORS
from flask_wtf.csrf import CSRFProtect

from app.bizlogic.database.engine import init_db
from app.bizlogic.errors import EnvironmentError, init_errors
from app.notification.logging import HTTPHandler, RedisHandler

load_dotenv(override=True)

csrf = CSRFProtect()

cors = CORS(resources={r"/api/*": {"origins": "*"}})

logconf.fileConfig(fname="logger.conf", disable_existing_loggers=False)

logging.getLogger("growthbook").setLevel(logging.CRITICAL)
logging.getLogger("urllib3.connectionpool").setLevel(logging.CRITICAL)
logging.getLogger("werkzeug").setLevel(logging.INFO)


http_handler = HTTPHandler(
    "http://localhost:5000/api/v1/log"
)  # Replace with your actual endpoint


REDIS_LOG_HANDLER = os.getenv("REDIS_LOG_HANDLER", "False") == "True"
if REDIS_LOG_HANDLER:
    redis_url = os.getenv("REDIS_URL", "redis://localhost:6379/0")
    redis_handler_root = RedisHandler(redis_url, "workpamfilico_logs_local")
    redis_handler_workpamfilico_sqlalchemy_logs_localsqlalchemy = RedisHandler(
        redis_url, "workpamfilico_sqlalchemy_logs_local"
    )
    root_logger = logging.getLogger()
    root_logger.addHandler(redis_handler_root)

    sqlalchemy_engine_logger = logging.getLogger("sqlalchemy.engine.Engine")
    sqlalchemy_engine_logger.addHandler(
        redis_handler_workpamfilico_sqlalchemy_logs_localsqlalchemy
    )


http_handler.setLevel(logging.INFO)
http_handler.setFormatter(
    logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")
)

logger = logging.getLogger(__name__)


SECRET_KEY = os.getenv("SECRET_KEY", "secret")
ENV = os.getenv("ENVIRONMENT", "local")


def create_app(testing_db_uri=""):
    app = Flask(__name__)

    app.config["SECRET_KEY"] = "secret"

    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
    app.config["SCHEDULER_API_ENABLED"] = True

    cors.init_app(app)

    csrf.init_app(app)

    csrf._exempt_views.add("dash.dash.dispatch")

    init_errors(app)

    db_uri = os.getenv("SQLALCHEMY_DATABASE_URI")

    if not db_uri:
        raise EnvironmentError("SQLALCHEMY_DATABASE_URI not defined")

    init_db(db_uri)

    from app.api.v1 import api as api_v1_blueprint

    app.register_blueprint(api_v1_blueprint, url_prefix="/api/v1")

    # flask_api_doc(
    #     app, config_path="app/swagger.json", url_prefix="/api/docs", title="API docs"
    # )

    # @app.route("/")
    # def goto_docs():
    #     return redirect("/api/docs")

    @app.route("/health")
    def goto_health():
        return "ok", 200

    return app
