import logging
import os

from growthbook import GrowthBook

logger = logging.getLogger(__name__)


def is_feature_enabled(flag_name: str):
    api_key = os.getenv("GROWTHBOOK_CLIENT_KEY")
    if not api_key:
        logger.error("GROWTHBOOK_CLIENT_KEY not set")
        return False

    def on_experiment_viewed(experiment, result):
        # TODO: Use your real analytics tracking system
        print("Viewed Experiment")
        print("Experiment Id: " + experiment.key)
        print("Variation Id: " + result.key)

    gb = GrowthBook(
        api_host="https://cdn.growthbook.io",
        client_key=api_key,
        on_experiment_viewed=on_experiment_viewed,
        cache_ttl=5,
    )

    gb.load_features()
    features = gb.getFeatures()
    feat = {}
    for k, _ in features.items():
        feat[k] = gb.is_on(k)

    activated = gb.is_on(flag_name)
    logger.info(f"Feature flag {flag_name} is activated: " + str(activated))
    return activated


def on_experiment_viewed(experiment, result):
    # TODO: Use your real analytics tracking system
    print("Viewed Experiment")
    print("Experiment Id: " + experiment.key)
    print("Variation Id: " + result.key)


def get_current_features(on_experiment_viewed=on_experiment_viewed):
    api_key = os.getenv("GROWTHBOOK_CLIENT_KEY")
    if not api_key:
        logger.error("GROWTHBOOK_CLIENT_KEY not set")
        return False

    gb = GrowthBook(
        api_host="https://cdn.growthbook.io",
        client_key=api_key,
        on_experiment_viewed=on_experiment_viewed,
        cache_ttl=5,
    )

    gb.load_features()
    features = gb.getFeatures()
    # features[0].to_dict()
    feat = {}
    for k, _ in features.items():
        feat[k] = gb.is_on(k)
    return feat
