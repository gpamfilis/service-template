from enum import Enum


class Role(Enum):
    ADMIN = "admin"
    USER = "user"
    STAFF = "staff"


class BookingType(Enum):
    EXISTING_BOOKING = "existing_booking"
    REQUEST = "request"
    AVAILABLE_SLOT = "available_slot"
