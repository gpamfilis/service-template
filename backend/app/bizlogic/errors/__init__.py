import logging
import traceback

from flask import jsonify
from marshmallow.exceptions import ValidationError
from sqlalchemy.exc import IntegrityError, OperationalError
from werkzeug.exceptions import HTTPException

from app.api.responses import standard_response
from app.api.responses.payment import payment_exists_error_response

logger = logging.getLogger(__name__)


class ForbidenError(Exception):
    pass


class ResourceExistsError(Exception):
    pass


class ResourceNotFoundError(Exception):
    pass


class AuthenticationError(Exception):
    pass


class UnknownException(Exception):
    # Here is a lazy exception.
    pass


class TokenError(Exception):
    pass


class EnvironmentError(Exception):
    pass


class NotAuthorizedToView(Exception):
    pass


# Car Errors


class CarIsCurrentlyBookedError(Exception):
    pass


class CarHasPendingBookingsError(Exception):
    pass


# Booking Errors
class BookingNotFoundError(Exception):
    pass


class BookingHasPaymentError(Exception):
    pass


# CUSTOMER ERRORS
class CustomerNotFoundError(Exception):
    pass


class CustomerHasBookingError(Exception):
    pass


class PaymentsExistError(Exception):
    pass


class PaymentHasBookingError(Exception):
    pass


def init_errors(app):
    @app.errorhandler(409)
    def conflict_error(error):
        logger.error(error)
        traceback.print_exc()
        return standard_response(
            success=False,
            message="Conflict",
            error=True,
            ui_message="Conflict",
            status_code=409,
        )

    @app.errorhandler(ResourceNotFoundError)
    def resource_not_found_error(error):
        msg = str(error)
        return jsonify({"error": True, "message": msg}), 404

    @app.errorhandler(AuthenticationError)
    def authentication_error(error):
        msg = str(error)
        return jsonify({"error": True, "message": msg}), 500

    @app.errorhandler(ValidationError)
    def validation_error(error):
        logger.error(error.messages)
        print(error.messages)
        return jsonify({"error": True, "message": error.messages}), 400

    @app.errorhandler(ResourceExistsError)
    def resource_exist_error(error):
        # logge
        logger.error(error)
        traceback.print_exc()
        return jsonify({"error": True, "message": str(error)}), 409

    @app.errorhandler(IntegrityError)
    def integrity_error(error):
        msg = str(error)

        return standard_response(
            success=False,
            message=msg,
            error=True,
            ui_message="Conflict",
            status_code=409,
        )

    @app.errorhandler(OperationalError)
    def operational_error(error):
        msg = str(error)
        logger.error(error)
        return standard_response(
            success=False,
            message=f"Database Error: {msg}",
            error=True,
            ui_message="Database Error",
            status_code=500,
        )

    @app.errorhandler(PaymentsExistError)
    def payment_exists_error(error):
        logger.error("Payment exists error: %s", error)
        response, status_code = payment_exists_error_response
        return jsonify(response), status_code

    @app.errorhandler(500)
    def server_error(error):
        logger.error(error)
        return jsonify({"error": True, "message": error}), 500

    debug = True  # global variable setting the debug config

    @app.errorhandler(Exception)
    def handle_exception(e):
        logger.error(e)
        traceback.print_exc()
        if isinstance(e, HTTPException):
            return e
        print(e)
        res = {
            "code": 500,
            "errorType": "Internal Server Error",
            "errorMessage": "Something went really wrong!",
        }
        if debug:
            res["errorMessage"] = e if hasattr(e, "message") else f"{e}"

        return jsonify(res), 500
