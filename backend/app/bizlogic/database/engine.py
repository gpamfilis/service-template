import logging
import os
from contextlib import contextmanager
from typing import Generator

from sqlalchemy.engine import create_engine
from sqlalchemy.engine.base import Engine
from sqlalchemy.exc import SQLAlchemyError

# from sqlalchemy.pool import NullPool
from sqlalchemy.orm import Session, scoped_session, sessionmaker

from app.bizlogic.database.models import Base

logger = logging.getLogger(__name__)


engine: Engine = None  # type: ignore
DBsession = sessionmaker(expire_on_commit=False)

SQLALCHEMY_ECHO = os.getenv("SQLALCHEMY_ECHO", "False").lower() == "true"


def init_db(url: str):
    engine = create_engine(
        url,
        # poolclass=NullPool,
        pool_pre_ping=True,  # Enable pre-ping to avoid stale connections
        pool_recycle=1800,  # Recycle connections after 1800 seconds
        echo=SQLALCHEMY_ECHO,
    )  # max_overflow=-1,
    Base.metadata.bind = engine  # type: ignore
    DBsession.bind = engine  # type: ignore


class LoggingSession:
    def __init__(self, log_message):
        self.log_message = log_message
        self.session = scoped_session(DBsession)

    def __enter__(self):
        logger.info(self.log_message)
        print("supper session")
        return self.session()

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.session.remove()


class LoggingSession2:
    def __init__(self, log_message, db_session_factory=DBsession):
        logger.info(log_message)
        self.session = scoped_session(db_session_factory)()

    def __getattr__(self, item):
        return getattr(self.session, item)


@contextmanager
def get_db_session() -> Generator[Session, None, None]:
    """
    Provide a transactional scope around a series of operations.

    Automatically commits the session after the block if no exceptions occur,
    rolls back if a SQLAlchemyError occurs, and logs and re-raises any exceptions.

    Yields:
        A SQLAlchemy Session object for database operations.
    """
    session = DBsession()
    try:
        yield session
        session.commit()
    except SQLAlchemyError as e:
        session.rollback()
        logger.exception(f"Session rollback because of exception {e}")
        raise
    except Exception as e:
        logger.exception(f"An unexpected error occurred with the DB session {e}")
        raise
    finally:
        session.close()
        logger.info("Database session closed.")
