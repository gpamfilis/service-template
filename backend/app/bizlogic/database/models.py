import logging
import uuid
from datetime import datetime

from itsdangerous import BadSignature, SignatureExpired
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer

# import pytz
from sqlalchemy import (
    Boolean,
    Column,
    ForeignKey,
    Integer,
    String,
    Text,
)
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import object_session, relationship
from sqlalchemy.sql import func
from werkzeug.security import check_password_hash, generate_password_hash

logger = logging.getLogger(__name__)

Base = declarative_base()


def generate_uuid():
    return str(uuid.uuid4())



class User(Base):
    __tablename__ = "user"
    id = Column(Integer, primary_key=True)
    username = Column(String(50), unique=True, nullable=False)
    phone_number = Column(String(50))
    pin_number = Column(String(10), unique=False, nullable=True)
    password_hash = Column(String(255), nullable=True)
    email = Column(String(100), unique=True, nullable=True)

    def __repr__(self):
        return f"User(id={self.id}, username={self.username})"

    @property
    def password(self):
        raise AttributeError("password is not a readable attribute")

    @password.setter
    def password(self, password):
        self.password_hash = generate_password_hash(password)

    def verify_password(self, password):
        return check_password_hash(self.password_hash, password)

    def generate_auth_token(self, expiration=100000):
        from app import SECRET_KEY

        serializer = Serializer(SECRET_KEY, expires_in=expiration)
        token = serializer.dumps({"user_id": self.id, "role": "manager"})
        return token

    def verify_auth_token(self, token):
        from app import SECRET_KEY

        serializer = Serializer(SECRET_KEY)
        session = object_session(self)
        try:
            data = serializer.loads(token)
        except SignatureExpired:
            return None  # valid token, but expired
        except BadSignature:
            return None  # invalid token
        user = session.query(User).get(data["user_id"])
        session.close()
        return user

    @hybrid_property
    def token(self):
        token = self.generate_auth_token()
        return token.decode("utf-8")



class Project(Base):  # this can also mean project?
    __tablename__ = "project"
    id = Column(Integer, primary_key=True)
    name = Column(String(100), nullable=False)
    description = Column(Text, nullable=True)
    user_id = Column(Integer, ForeignKey("user.id"), nullable=False)

    # Relationships
    user = relationship("User", backref="projects")
    
    show_in_flow = Column(Boolean, default=False)
    show_in_apps = Column(Boolean, default=False)
    show_in_payments = Column(Boolean, default=False)



class TeamMember(Base):
    __tablename__ = "teammember"

    id = Column(Integer, primary_key=True)
    # Add other necessary fields as required
    user_id = Column(Integer, ForeignKey("user.id"), nullable=False)
    phone_number = Column(String(50))
    email = Column(String(100), unique=True, nullable=True)
    role = Column(String(100), nullable=False)
    description = Column(Text, nullable=True)
    full_name = Column(String(100), nullable=True)
    member_type = Column(String(100), nullable=True)
    deleted = Column(Boolean, default=False)

    pin_number = Column(String(10), unique=False, nullable=True)
    password_hash = Column(String(255), nullable=True)

    # Many-to-Many relationship with Task


    # Relationships
    team_member_roles = relationship("TeamMemberRole", back_populates="team_member")
    permissions = relationship("Permission", back_populates="team_member")

    # Assuming the 'tasks' relationship and task_teammember_association are defined elsewhere

    @property
    def password(self):
        raise AttributeError("password is not a readable attribute")

    @password.setter
    def password(self, password):
        self.password_hash = generate_password_hash(password)

    def verify_password(self, password):
        return check_password_hash(self.password_hash, password)

    def generate_auth_token(self, expiration=100000):
        from app import SECRET_KEY

        serializer = Serializer(SECRET_KEY, expires_in=expiration)
        token = serializer.dumps({"user_id": self.id, "role": "member"})
        return token

    def verify_auth_token(self, token):
        from app import SECRET_KEY

        serializer = Serializer(SECRET_KEY)
        session = object_session(self)
        try:
            data = serializer.loads(token)
        except SignatureExpired:
            return None  # valid token, but expired
        except BadSignature:
            return None  # invalid token
        user = session.query(TeamMember).get(data["user_id"])
        session.close()
        return user

    @hybrid_property
    def token(self):
        token = self.generate_auth_token()
        return token.decode("utf-8")


class TeamMemberRole(Base):
    __tablename__ = "teammemberrole"

    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey("user.id"), nullable=False)
    role_name = Column(String(64), nullable=False)

    # Relationship back to TeamMember
    team_member_id = Column(Integer, ForeignKey("teammember.id"), nullable=False)
    team_member = relationship("TeamMember", back_populates="team_member_roles")


class Permission(Base):
    __tablename__ = "permission"

    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey("user.id"), nullable=False)
    add_todo = Column(Boolean, default=False)
    edit_todo = Column(Boolean, default=False)

    # Relationship back to TeamMember
    team_member_id = Column(Integer, ForeignKey("teammember.id"), nullable=False)
    team_member = relationship("TeamMember", back_populates="permissions")
