# Define allowed roles for TeamMemberRole
from enum import Enum


class RoleEnum(Enum):
    ADMIN = "Admin"
    MANAGER = "Manager"
    DEVELOPER = "Developer"
    DESIGNER = "Designer"

    # Add other roles as needed
