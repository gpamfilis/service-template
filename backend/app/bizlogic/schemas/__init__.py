import types
from typing import TypedDict

from marshmallow import EXCLUDE, Schema, fields


class UserSchema(Schema):
    class Meta:
        EXCLUDE = True

    id = fields.Int()
    username = fields.Str()


class CustomerSchema(Schema):
    class Meta:
        EXCLUDE = True

    id = fields.Int()
    user_id = fields.Int(required=True)
    first_name = fields.Str(required=True)
    last_name = fields.Str()
    email = fields.Email()
    phone_number = fields.Str()
    has_phone_number = fields.Boolean()


class CreateCustomerSchema(Schema):
    class Meta:
        EXCLUDE = True

    user_id = fields.Int(required=True)
    first_name = fields.Str(required=True)
    last_name = fields.Str()
    email = fields.Email()
    phone_number = fields.Str()


class EditCustomerSchema(Schema):
    class Meta:
        unknown = EXCLUDE

    first_name = fields.Str(required=True)
    last_name = fields.Str(allow_none=True)
    email = fields.Str(allow_none=True)
    phone_number = fields.Str()


class ServiceLogSchema(Schema):
    class Meta:
        EXCLUDE = True

    id = fields.Int()
    car_id = fields.Int(required=True)
    description = fields.Str()
    service_date = fields.DateTime()


class CarPricingSchema(Schema):
    class Meta:
        EXCLUDE = True

    id = fields.Int()
    month = fields.Int()
    price_per_day = fields.Float()


def schema_to_dict(schema):
    # Initialize an empty dictionary to store the schema as a dictionary
    schema_dict = {}

    # Iterate over the fields in the schema and convert them to a dictionary
    for field_name, field_obj in schema().fields.items():
        field_info = {
            "type": field_obj.__class__.__name__,  # Get the field type (e.g., 'Str', 'Int', etc.)
            "required": field_obj.required,  # Check if the field is required
            "metadata": field_obj.metadata,  # Get any additional metadata associated with the field
        }
        schema_dict[field_name] = field_info
    return schema_dict


class GetPaymentSchema(Schema):
    class Meta:
        EXCLUDE = True

    # customer_id = fields.Int()
    id = fields.Int()
    date = fields.Date()
    amount = fields.Float()
    payment_type = fields.Str()
    note = fields.Str()
    customers = fields.Nested(lambda: CustomerSchema())
    is_deposit = fields.Boolean()
    created_by = fields.Str()
    create_by_id = fields.Int()
    create_by_role = fields.Str()
    create_date = fields.Str()
    timestamp = fields.DateTime()


class CreatePaymentSchema(Schema):
    class Meta:
        unknown = EXCLUDE

    customer_id = fields.Int()
    booking_id = fields.Int()
    # user_id = fields.Int()
    amount = fields.Float()
    payment_type = fields.Str()
    note = fields.Str()
    is_deposit = fields.Boolean()
    # Add validation from the enum
    create_by_role = fields.Str(required=False)
    create_by_id = fields.Int(required=False)


class EditPaymentSchema(Schema):
    class Meta:
        unknown = EXCLUDE

    amount = fields.Float()
    payment_type = fields.Str()
    note = fields.Str(allow_none=True)
    is_deposit = fields.Boolean()
    create_by_role = fields.Str(required=False)
    create_by_id = fields.Int(required=False)


def schema_to_TypedDict(schema):
    def schema_to_dict(schema):
        schema_dict = {}
        for field_name, field_obj in schema().fields.items():
            field_info = {
                "type": field_obj.__class__.__name__,
                "required": field_obj.required,
                "metadata": field_obj.metadata,
            }
            schema_dict[field_name] = field_info
        return schema_dict

    schema_dict = schema_to_dict(schema)

    def map_field_to_type(field_type: str):
        type_mapping = {
            "Int": int,
            "Str": str,
            "Boolean": bool,
            "Dict": dict,
            # Add other type mappings as needed
        }
        return type_mapping.get(field_type, object)

    def create_typeddict_from_schema(schema_dict, class_name):
        annotations = {
            field_name: map_field_to_type(info["type"])
            for field_name, info in schema_dict.items()
        }
        # Create the TypedDict using types.new_class
        return types.new_class(
            class_name,
            (TypedDict,),
            exec_body=lambda ns: ns.update(__annotations__=annotations),
        )

    # Assuming schema_dict is correctly populated
    GetCarTypedDict = create_typeddict_from_schema(schema_dict, "GetCarTypedDict")

    # Test the output
    return GetCarTypedDict.__annotations__
