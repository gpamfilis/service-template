from marshmallow import EXCLUDE, Schema, fields, post_load


class CreateUserSchema(Schema):
    class Meta:
        unknown = EXCLUDE

    email = fields.Str(required=True)
    password = fields.Str(required=True)

    @post_load
    def parse_username(self, data, **kwargs):
        data["username"] = data["email"].split("@")[0]
        return data


class GetUserSchema(Schema):
    class Meta:
        unknown = EXCLUDE

    email = fields.Str(required=True)


class EventUserSchema(Schema):
    username = fields.Str(required=True)
    id = fields.Int(required=True)
    email = fields.Str(required=True)


class GetUserTokenSchema(Schema):
    class Meta:
        unknown = EXCLUDE

    email = fields.Str(required=True)
    password = fields.Str(required=True)


class UserTokenSchema(Schema):
    class Meta:
        unknown = EXCLUDE

    id = fields.Int()
    role = fields.Str(required=True)
    token = fields.Str(required=True)
