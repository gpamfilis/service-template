import datetime
import time


def time_elapsed(func):
    def wrapper(*args, **kwargs):
        start_time = time.time()  # Record the start time
        result = func(*args, **kwargs)  # Call the function
        end_time = time.time()  # Record the end time
        print(f"{func.__name__} took {end_time - start_time:.2f} seconds to execute.")
        return result

    return wrapper


def convert_am_pm_to_24_hour(time_str):
    """
    Convert a time string from AM/PM format or 24-hour format to 24-hour format.

    Args:
        time_str (str): The time string in either 'YYYY-MM-DD HH:MM:SS AM/PM' or 'YYYY-MM-DD HH:MM:SS' format.

    Returns:
        datetime.datetime: A datetime object in 24-hour format.

    Example:
        >>> convert_am_pm_to_24_hour("2023-09-12 5:00:44 PM")
        datetime.datetime(2023, 9, 12, 17, 0, 44)
        >>> convert_am_pm_to_24_hour("2023-09-16 12:00:00")
        datetime.datetime(2023, 9, 16, 12, 0)
    """
    # Define a dictionary for AM/PM conversion
    am_pm_dict = {"AM": "am", "PM": "pm"}

    # Check if the input string contains AM/PM format
    has_am_pm = any(am_pm in time_str for am_pm in am_pm_dict)

    # Replace AM/PM with lowercase equivalents
    for am_pm, replacement in am_pm_dict.items():
        time_str = time_str.replace(am_pm, replacement)

    try:
        # Parse the datetime string with the correct format
        if has_am_pm:
            return datetime.datetime.strptime(time_str, "%Y-%m-%d %I:%M:%S %p")
        else:
            return datetime.datetime.strptime(time_str, "%Y-%m-%d %H:%M:%S")
    except Exception as e:
        print("converted exception", e)
        return time_str
