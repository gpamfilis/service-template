from sqlalchemy.exc import IntegrityError

from app.bizlogic.errors import ResourceNotFoundError


def create_obj(model, data, session):
    obj = model(**data)
    session.add(obj)
    try:
        session.commit()
        return obj
    except IntegrityError:
        session.rollback()
        raise


def edit_item_by_id(
    model, data, item_id, session, msg="No Car found with the id provided"
):
    obj = session.query(model).get(item_id)
    if not obj:
        raise ResourceNotFoundError(f"{msg}: {item_id}")
    # Here we loop over the payload and change the car attributes.
    # instead of car.license_plate = 'dfadsfs' we loop over them.
    for k, v in data.items():
        setattr(obj, k, v)
    session.add(obj)
    try:
        session.commit()
        return obj
    except IntegrityError:
        session.rollback()
        raise


def delete_obj_by_id(model, item_id, session, msg="No Car found with the id provided"):
    obj = session.query(model).get(item_id)
    if not obj:
        raise ResourceNotFoundError(f"{msg}: {item_id}")
    session.delete(obj)
    session.commit()
