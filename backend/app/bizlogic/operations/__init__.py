import logging
from abc import ABC, abstractmethod

logger = logging.getLogger(__name__)


class Manager(ABC):
    """
    Abstract class for model operations.
    """

    @abstractmethod
    def __init__(self, session) -> None:
        self.session = session

    @abstractmethod
    def get_item_by_id(self, item_id: int):
        "Get item by id"

    @abstractmethod
    def get_items(self):
        "Get items"

    @abstractmethod
    def create_item(self, data: dict):
        "Create item"

    @abstractmethod
    def edit_item(self, item_id: int, data: dict):
        "Edit item"

    @abstractmethod
    def delete_item(self, item_id: int):
        "Delete item"

    def _update_item(self, obj, data: dict, disallowed_fields: list = []):
        for k, v in data.items():
            if k in disallowed_fields:
                logger.warning(f"Field {k} not allowed")
                continue
            setattr(obj, k, v)
        self.session.add(obj)
        self.session.commit()
        return obj
