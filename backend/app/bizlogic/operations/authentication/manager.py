from app.bizlogic.database.models import Staff, User
from app.bizlogic.errors import ResourceNotFoundError
from app.bizlogic.operations import Manager


class AuthenticationManager(Manager):
    def __init__(self, session, user_id=None) -> None:
        self.session = session
        self.user_id = user_id

    def get_user_id(self, auth):
        if auth["role"] == "staff":
            staff = (
                self.session.query(Staff)
                .filter(Staff.user_id == auth["user_id"])
                .first()
            )
            if staff is None:
                raise ResourceNotFoundError(f"No staff with id {auth['user_id']}")
            user_id = staff.user_id
        else:
            user_id = auth["user_id"]
            user = self.session.query(User).get(user_id)
            if user is None:
                raise ResourceNotFoundError(f"No User with id {user_id}")
        return user_id

    def get_item_by_id(self, item_id: int):
        return ""

    def get_items(self):
        return []

    def create_item(self, data):
        return ""

    def edit_item(self, item_id, data):
        return ""

    def delete_item(self, item_id):
        return ""
