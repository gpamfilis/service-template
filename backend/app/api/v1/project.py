from flask import jsonify, request

from app import csrf
from app.api.decorators import user_scope
from app.api.responses import standard_response
from app.api.schemas.project import ProjectCreate, ProjectEdit, ProjectGet
from app.bizlogic.database.engine import DBsession
from app.bizlogic.database.models import Project, TeamMember

from . import api


@api.route("/project", methods=["GET"])
@user_scope
def list_projects(auth):
    user_id = auth["user_id"]
    session = DBsession()
    if auth["role"] == "manager":
        projects = session.query(Project).filter_by(user_id=user_id).all()
        objs = ProjectGet().dump(projects, many=True)
        session.close()
        return jsonify(objs)
    
    member = session.query(TeamMember).get(auth["user_id"])
    user_id = member.user_id

    projects = session.query(Project).filter_by(user_id=user_id).all()
    objs = ProjectGet().dump(projects, many=True)
    session.close()
    return jsonify(objs)


@api.route("/project", methods=["POST"])
@csrf.exempt
@user_scope
def create_project(auth):
    session = DBsession()
    data = ProjectCreate().load(request.get_json())
    data["user_id"] = auth["user_id"]
    new_task = Project(**data)
    session.add(new_task)
    session.commit()
    new_task_id = new_task.id
    session.close()
    return jsonify({"message": "Group created", "task_id": new_task_id}), 201


@api.route("/project/<int:project_id>", methods=["PUT"])
@csrf.exempt
@user_scope
def edit_project(project_id, auth):
    session = DBsession()
    data = ProjectEdit().load(request.get_json())
    project = (
        session.query(Project).filter_by(id=project_id, user_id=auth["user_id"]).first()
    )

    if project:
        for key, value in data.items():
            setattr(project, key, value)
        session.commit()
        session.close()
        return jsonify({"message": "Project updated"}), 200
    else:
        session.close()
        return jsonify({"error": "Project not found"}), 404

@api.route("/project/<int:project_id>", methods=["DELETE"])
@csrf.exempt
@user_scope
def delete_project(project_id, auth):
    session = DBsession()
    project = (
        session.query(Project).filter_by(id=project_id, user_id=auth["user_id"]).first()
    )
    if project:
        session.delete(project)
        session.commit()
        session.close()
        return jsonify({"message": "Project deleted"}), 200
    else:
        session.close()
        return jsonify({"error": "Project not found"}), 404
