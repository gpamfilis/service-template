from flask import jsonify, request

from app import csrf
from app.api.decorators import user_scope
from app.api.schemas.teammember import TeamMemberCreate, TeamMemberEdit, TeamMemberGet
from app.bizlogic.database.engine import DBsession, LoggingSession2
from app.bizlogic.database.models import TeamMember
from app.bizlogic.errors import ResourceNotFoundError
from app.bizlogic.schemas.user import GetUserTokenSchema, UserTokenSchema

from . import api


@api.route("/teammember", methods=["GET"])
@user_scope
def list_members(auth):
    session = DBsession()
    groups = (
        session.query(TeamMember)
        .filter_by(user_id=auth["user_id"])
        .filter((TeamMember.deleted == False) | (TeamMember.deleted == None))
        .all()
    )
    objs = TeamMemberGet().dump(groups, many=True)
    session.close()
    return jsonify(objs)


@api.route("/teammember/<int:id>", methods=["GET"])
@user_scope
def get_member(id, auth):
    session = DBsession()
    member = (
        session.query(TeamMember)
        .filter(TeamMember.id == id)
        .filter_by(user_id=auth["user_id"])
        .first()
    )
    obj = TeamMemberGet().dump(member)
    session.close()
    return jsonify(obj)


@api.route("/teammember", methods=["POST"])
@csrf.exempt
@user_scope
def create_member(auth):
    session = DBsession()
    data = request.get_json()
    obj = TeamMemberCreate().load(data)
    obj["role"] = "member"
    member = TeamMember(**obj)
    member.user_id = auth["user_id"]
    session.add(member)
    session.commit()
    obj = TeamMemberGet().dump(member)
    session.close()
    return jsonify(obj)


@api.route("/teammember/<int:id>", methods=["PUT"])
@csrf.exempt
@user_scope
def update_member(id, auth):
    session = DBsession()
    data = request.get_json()
    obj = TeamMemberEdit().load(data)
    member = (
        session.query(TeamMember)
        .filter(TeamMember.id == id)
        .filter_by(user_id=auth["user_id"])
        .first()
    )
    for key, value in obj.items():
        setattr(member, key, value)
    session.commit()
    obj = TeamMemberGet().dump(member)
    session.close()
    return jsonify(obj)


@api.route("/teammember/<int:id>", methods=["DELETE"])
@csrf.exempt
@user_scope
def delete_member(id, auth):
    session = DBsession()
    member = (
        session.query(TeamMember)
        .filter(TeamMember.id == id)
        .filter_by(user_id=auth["user_id"])
        .first()
    )
    member.deleted = True
    # session.delete(member)
    session.commit()
    session.close()
    return jsonify({"status": "success"})


@api.route("/teammember/token", methods=["POST"])
@csrf.exempt
def teammember_generate_token():
    """
    ---
    post:
      summary: Get User Token.
      tags:
        - User
      parameters:
      - in: query
        schema: TokenParameter
      security:
        - ApiKeyAuth: []
      requestBody:
          content:
            application/json:
              schema: GetUserTokenSchema
      responses:
        200:
          content:
            application/json:
              schema: UserTokenSchema
    """
    data = GetUserTokenSchema().load(request.get_json(force=True))

    session = LoggingSession2("User token")

    def generate_user_token(data, session):
        username = data["email"]
        user: TeamMember = session.query(TeamMember).filter_by(email=username).first()
        if user is None:
            raise ResourceNotFoundError()
        elif user.verify_password(data["password"]):
            return user

    user = generate_user_token(data, session)
    if user is None:
        session.close()
        raise ResourceNotFoundError("No staff with that phone number and pin number")
    permissions = {
        "can_view_flow": False,
        "can_view_notes": False,
        "can_view_team": False,
        "can_view_members": False,
        "can_view_canvas": False,
        "can_view_apps": False,
        "add_app": False,
        "edit_app": False,
        "can_view_projects": False,
    }
    user_obj = UserTokenSchema().dump(user)
    user_obj["permissions"] = permissions
    session.close()
    return user_obj, 200
