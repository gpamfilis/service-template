import logging

from flask import jsonify, request

from app import csrf  # type: ignore
from app.api.decorators import user_scope
from app.api.responses import no_content, standard_response
from app.bizlogic.database.engine import DBsession, LoggingSession2
from app.bizlogic.database.models import TeamMember, User
from app.bizlogic.errors import ResourceNotFoundError
from app.bizlogic.operations.common import create_obj, delete_obj_by_id
from app.bizlogic.schemas import UserSchema
from app.bizlogic.schemas.user import (
    CreateUserSchema,
    GetUserSchema,
    GetUserTokenSchema,
    UserTokenSchema,
)

from . import api

logger = logging.getLogger(__name__)


@api.route("/user", methods=["GET"])
def list_users():
    session = DBsession()
    objs = session.query(User).all()
    user = UserSchema().dump(objs, many=True)
    session.close()
    return jsonify(user)


@api.route("/user/register", methods=["POST"])
@csrf.exempt
def create_user():
    """
    ---
    post:
      summary: Create a user.
      tags:
        - User
      security:
        - ApiKeyAuth: []
      requestBody:
          content:
            application/json:
              schema: CreateUserSchema
      responses:
        201:
          content:
            application/json:
              schema: GetUserSchema
    """
    data = CreateUserSchema().load(request.get_json())
    session = DBsession()

    email = data["email"]
    user = session.query(User).filter_by(email=email).first()
    if user is not None:
        session.close()
        return standard_response(
            data=None, status_code=400, ui_message="User already exists"
        )
    user = create_obj(User, data, session)

    obj = GetUserSchema().dump(user)
    session.close()
    return standard_response(
        data=obj, status_code=201, ui_message="User created successfully"
    )


@api.route("/user/<int:user_id>", methods=["DELETE"])
@csrf.exempt
def delete_user(user_id):
    """
    ---
    delete:
      summary: Delete a User.
      tags:
        - User
      parameters:
      - in: path
        schema: UserIDParameter
      security:
        - ApiKeyAuth: []
      responses:
        204:
          content:
            application/json:
              schema: NoContentSchema
    """

    session = DBsession()
    delete_obj_by_id(User, user_id, session=session)
    session.close()
    return no_content()


@api.route("/user/token", methods=["POST"])
@csrf.exempt
def user_generate_token():
    """
    ---
    post:
      summary: Get User Token.
      tags:
        - User
      parameters:
      - in: query
        schema: TokenParameter
      security:
        - ApiKeyAuth: []
      requestBody:
          content:
            application/json:
              schema: GetUserTokenSchema
      responses:
        200:
          content:
            application/json:
              schema: UserTokenSchema
    """
    data = GetUserTokenSchema().load(request.get_json(force=True))

    session = LoggingSession2("User token")

    def generate_user_token(data, session):
        username = data["email"]
        user: User = session.query(User).filter_by(email=username).first()
        if user is None:
            # TODO: 90c689d8-255b-45c4-a534-bbc7d7579c07 - restore this line when 9f551c99-9341-4cfe-bd2c-72de80b253bd is complete.
            # raise ResourceNotFoundError()
            return None
        elif user.verify_password(data["password"]):
            return user

    def generate_member_token(data, session):
        username = data["email"]
        user: TeamMember = session.query(TeamMember).filter_by(email=username).first()
        if user is None:
            return None
        elif user.verify_password(data["password"]):
            return user

    user = generate_user_token(data, session)
    if user is None:
        session.close()
        raise ResourceNotFoundError("No staff with that phone number and pin number")
    user_obj = UserTokenSchema().dump(user)
    permissions = {
        "can_view_flow": True,
        "can_view_notes": True,
        "can_view_team": True,
        "can_view_members": True,
        "can_view_canvas": True,
        "can_view_apps": True,
        "add_app": True,
        "edit_app": True,
        "can_view_payments": True,
        "can_view_projects": True,
    }
    user_obj["permissions"] = permissions
    session.close()
    return user_obj, 200


@api.route("/user/token/test")
@user_scope
def user_test(auth):
    return "ok"
