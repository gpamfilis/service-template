from marshmallow import EXCLUDE, Schema, fields, post_dump


class ProjectCreate(Schema):
    class Meta:
        unknown = EXCLUDE

    name = fields.Str()
    description = fields.Str()
    show_in_flow = fields.Bool()
    show_in_apps = fields.Bool()
    show_in_payments = fields.Bool()


class ProjectGet(Schema):
    id = fields.Str()
    name = fields.Str()
    description = fields.Str()
    show_in_flow = fields.Bool()
    show_in_apps = fields.Bool()
    show_in_payments = fields.Bool()

    @post_dump
    def if_show_in_flow_none(self, data, **kwargs):
        if data["show_in_flow"] is None:
            data["show_in_flow"] = False
        return data

    @post_dump
    def if_show_in_apps_none(self, data, **kwargs):
        if data["show_in_apps"] is None:
            data["show_in_apps"] = False
        return data

    @post_dump
    def if_show_in_payments_none(self, data, **kwargs):
        if data["show_in_payments"] is None:
            data["show_in_payments"] = False
        return data


class ProjectEdit(Schema):
    class Meta:
        unknown = EXCLUDE

    name = fields.Str(required=True)
    description = fields.Str()
    show_in_flow = fields.Bool()
    show_in_apps = fields.Bool()
    show_in_payments = fields.Bool()
