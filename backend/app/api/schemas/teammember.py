from marshmallow import EXCLUDE, Schema, fields, post_load

from app.api.schemas.permission import PermissionSchema


class TeamMemberRoleSchema(Schema):
    id = fields.Int()
    role_name = fields.Str()


class TeamMemberGet(Schema):
    id = fields.Int()
    phone_number = fields.Str()
    email = fields.Str()
    role = fields.Str()
    description = fields.Str()
    full_name = fields.Str()
    member_type = fields.Str(allow_none=True)
    team_member_roles = fields.List(fields.Nested(TeamMemberRoleSchema))
    permissions = fields.List(fields.Nested(PermissionSchema))


class TeamMemberCreate(Schema):
    class Meta:
        unknown = EXCLUDE

    phone_number = fields.Str()
    description = fields.Str()
    full_name = fields.Str()
    member_type = fields.Str()


class TeamMemberEdit(Schema):
    class Meta:
        unknown = EXCLUDE

    phone_number = fields.Str()
    description = fields.Str()
    full_name = fields.Str()
    email = fields.Str()
    member_type = fields.Str()

    @post_load
    def empty_email_to_none(self, data, **kwargs):
        if data["email"] == "":
            data["email"] = None
        return data
