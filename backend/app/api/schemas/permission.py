from marshmallow import Schema, fields


class PermissionSchema(Schema):
    id = fields.Int()
    user_id = fields.Int()
    add_todo = fields.Bool()
    edit_todo = fields.Bool()
