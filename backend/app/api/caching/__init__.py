import json
import logging
import os

import redis

from app import ENV
from app.api.v1.booking import resource_timeline_operation
from app.bizlogic.database.engine import get_db_session
from app.bizlogic.operations.booking.manager import BookingManager

logger = logging.getLogger(__name__)


def cache_latest_bookings(user_id=1, logger=None):
    """
    Caches the latest bookings for a given user in Redis.

    :param user_id: The ID of the user whose bookings to cache. Defaults to 1.
    :type user_id: int
    :param logger: The logger to use for logging messages. Required.
    :type logger: logging.Logger
    :raises ValueError: If no logger is provided.
    :raises Exception: If there is an error deleting or setting the cache key.
    """
    cache = redis.Redis.from_url(os.environ.get("REDIS_URL"))

    if not logger:
        raise ValueError("No logger provided for cache_latest_bookings function")

    logger.info("Caching latest bookings for user_id=%s", user_id)
    with get_db_session() as session:
        bm = BookingManager(session=session, user_id=user_id)
        data = bm.get_items()
        logger.info(f"Cached Bookings: size={len(data)}")

    cache_key = f"{ENV}_booking_list_user_{user_id}"
    logger.info(f"Checking if cache key exists: {cache_key}")

    if cache.exists(cache_key):
        logger.info(f"Deleting existing cache key: {cache_key}")
        cache.delete(cache_key)
        if cache.exists(cache_key):
            logger.error(f"Failed to delete cache key: {cache_key}")
            raise Exception(f"Failed to delete cache key: {cache_key}")
        else:
            logger.info(f"Cache key deleted successfully: {cache_key}")
    else:
        logger.info(f"Cache key does not exist, no need to delete: {cache_key}")

    logger.info(f"Setting new cache key: {cache_key}")
    try:
        cache.set(cache_key, json.dumps(data))
        logger.info(f"Cache key set successfully: {cache_key}")
    except Exception as e:
        logger.error(f"Failed to set cache key {cache_key}: {e}")
        raise


def cache_latest_resource_timeline(user_id=1, logger=None):
    """
    Caches the latest bookings for a given user in Redis.

    :param user_id: The ID of the user whose bookings to cache. Defaults to 1.
    :type user_id: int
    :param logger: The logger to use for logging messages. Required.
    :type logger: logging.Logger
    :raises ValueError: If no logger is provided.
    :raises Exception: If there is an error deleting or setting the cache key.
    """
    cache = redis.Redis.from_url(os.environ.get("REDIS_URL"))

    if not logger:
        raise ValueError("No logger provided for cache_latest_bookings function")

    logger.info("Caching latest bookings for user_id=%s", user_id)
    with get_db_session() as session:
        data = resource_timeline_operation(
            user_id=user_id, session=session, custom_logger=logger
        )
        logger.info(f"Cached Bookings: size={len(data)}")

    cache_key = f"{ENV}_resource_timeline_{user_id}"
    logger.info(f"Checking if cache key exists: {cache_key}")

    if cache.exists(cache_key):
        logger.info(f"Deleting existing cache key: {cache_key}")
        cache.delete(cache_key)
        if cache.exists(cache_key):
            logger.error(f"Failed to delete cache key: {cache_key}")
            raise Exception(f"Failed to delete cache key: {cache_key}")
        else:
            logger.info(f"Cache key deleted successfully: {cache_key}")
    else:
        logger.info(f"Cache key does not exist, no need to delete: {cache_key}")

    logger.info(f"Setting new cache key: {cache_key}")
    try:
        cache.set(cache_key, json.dumps(data))
        logger.info(f"Cache key set successfully: {cache_key}")
    except Exception as e:
        logger.error(f"Failed to set cache key {cache_key}: {e}")
        raise
