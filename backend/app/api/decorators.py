from functools import partial, wraps

from flask import request
from itsdangerous import BadSignature, SignatureExpired
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer

from app import SECRET_KEY
from app.bizlogic.database.engine import DBsession
from app.bizlogic.database.models import TeamMember, User
from app.bizlogic.errors import AuthenticationError, NotAuthorizedToView


# TODO: raise error if user is not verified.
def verify_auth_token_return_id(token):
    if not token:
        raise AuthenticationError("Please provide a token.")
    session = DBsession()
    serializer = Serializer(SECRET_KEY)

    try:
        data = serializer.loads(token)

        role = data["role"]
        if role == "manager":
            user = session.query(User).get(data["user_id"])
            session.close()
            return role, user.id
        if role == "member":
            member = session.query(TeamMember).get(data["user_id"])
            session.close()
            return role, member.id
        else:
            session.rollback()
            session.close()
            raise AuthenticationError("Invalid Token.")
    except SignatureExpired:
        session.rollback()
        session.close()
        raise AuthenticationError("Your token has expired. Please try again.")
    except BadSignature:
        session.rollback()
        session.close()
        raise AuthenticationError("Invalid Token.")


def token_required(f, scopes=[]):
    @wraps(f)
    def decorator(*args, **kwargs):
        # !! TODO: move token to header
        token = request.args.get("token", None, str)
        role, id = verify_auth_token_return_id(token)
        if role == "manager":
            kwargs["auth"] = {"role": role, "user_id": id}
            return f(*args, **kwargs)
        if role == "member":
            kwargs["auth"] = {"role": role, "user_id": id}
        if role not in scopes:
            raise NotAuthorizedToView("Role not in allowed scopes.")
        return f(*args, **kwargs)

    return decorator


user_scope = partial(token_required, scopes=["manager", "member"])
admin_scope = partial(token_required, scopes=["admin"])
staff_scope = partial(token_required, scopes=["staff"])
all_scope = partial(token_required, scopes=["staff", "user"])
