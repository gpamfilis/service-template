from typing import Dict, Optional, Tuple, Union

RESPONSE_TYPE = Tuple[
    Dict[
        str,
        Optional[Union[bool, str, Dict[str, Union[bool, str, None]]]],
    ],
    int,
]  # example {"error": False, "msg": "no content", "data": {...}}, 204


def no_content(data: Optional[dict] = None) -> RESPONSE_TYPE:
    return {"error": False, "msg": "no content", "data": data}, 204


def not_found() -> RESPONSE_TYPE:
    return {"error": False, "msg": "resource not found"}, 404


def ok() -> RESPONSE_TYPE:
    return {"error": False, "msg": "ok"}, 200


def created(msg: str | dict = "ok") -> RESPONSE_TYPE:
    return {"error": False, "msg": msg}, 201


def standard_response(
    data=None,
    ui_message="",
    status_code: int = 200,
    redirect_to_login: bool = False,
    excluded_keys=[
        "pagination",
        "meta",
        "rateLimit",
        "_links",
        "requestInfo",
        "debugInfo",
        "warnings",
        "locale",
        "timezone",
        "authToken",
    ],
    error=False,
    success=True,
    message="",
):
    """
    Generates a standard response dictionary with various fields and a status code.

    Parameters:
    - data (optional): The data to be included in the response. Defaults to None.
    - ui_message (str, optional): A user interface message. Defaults to an empty string.
    - status_code (int, optional): The HTTP status code for the response. Defaults to 200.
    - excluded_keys (list, optional): A list of keys to exclude from the response. Defaults to None.

    Returns:
    - tuple: A tuple containing the response dictionary and the status code.

    Doctests:
    >>> response, code = standard_response(data={"item": "value"}, ui_message="Test Message", status_code=200)
    >>> response["ui_message"]
    'Test Message'
    >>> response["data"]
    {'item': 'value'}
    >>> code
    200

    >>> response, _ = standard_response(excluded_keys=["data", "meta"])
    >>> "data" in response
    False
    >>> "meta" in response
    False
    """

    if excluded_keys is None:
        excluded_keys = []

    response_template = {
        "success": success,
        "message": message,
        "error": error,
        "redirect_to_login": redirect_to_login,
        "ui_message": ui_message,
        "status_code": status_code,
        "data": data,
        "pagination": {
            "currentPage": None,
            "totalPages": None,
            "pageSize": None,
            "totalCount": None,
            "nextPage": None,
            "previousPage": None,
        },
        "meta": {"apiVersion": None, "responseTime": None, "fromCache": None},
        "rateLimit": {
            "limit": None,
            "remaining": None,
            "reset": None,
        },
        "warnings": [],
        "locale": None,
        "timezone": None,
        "authToken": None,
        "_links": {
            "self": None,
            "next": None,
            "previous": None,
        },
        "requestInfo": {},
        "debugInfo": {"stackTrace": None},
    }

    # Remove excluded keys
    for key in excluded_keys:
        response_template.pop(key, None)

    return response_template, status_code
