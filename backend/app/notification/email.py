import datetime
import logging
import smtplib
from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from io import BytesIO

import pandas as pd

from app.backup.dataframe import generate_backup_df
from app.bizlogic.database.engine import DBsession
from app.bizlogic.database.models import User
from app.bizlogic.schemas.user import EventUserSchema

logger = logging.getLogger(__name__)


def send_backup_email_to_user(data, custom_logger=None):
    if custom_logger:
        logger = custom_logger
    user_id = data["user_id"]
    logger.info("Starting send_email_with_attachment")

    session = DBsession()
    user = session.query(User).filter_by(id=user_id).first()
    user = EventUserSchema().dump(user)
    logger.info(f"Fetched User: {user}")

    # Create the DataFrame and style it
    styled_df = generate_backup_df(user["id"])
    # Create a BytesIO buffer and save the styled DataFrame to it
    buffer = BytesIO()
    with pd.ExcelWriter(buffer, engine="openpyxl") as writer:
        styled_df.to_excel(writer, sheet_name="ALL MONTHS", index=True)
    buffer.seek(0)

    # Email parameters
    smtp_server = "smtp.gmail.com"
    smtp_port = 587
    sender_email = "gpamfilis@gmail.com"
    sender_password = "gpdawxoepemsxbnz"
    receiver_email = user["email"]
    subject = f"Backup rentalman.app {datetime.datetime.now()}"

    # Initialize the message
    message = MIMEMultipart()
    message["From"] = sender_email
    message["To"] = receiver_email
    message["Subject"] = subject

    # Attach the body text
    body_text = (
        f"Hello {user['username']} this email contains an your "
        "booking scedule backup as an excell attachment."
    )
    message.attach(MIMEText(body_text, "plain"))

    # Attach the Excel file
    part = MIMEBase("application", "octet-stream")
    part.set_payload(buffer.read())
    encoders.encode_base64(part)
    part.add_header(
        "Content-Disposition",
        f'attachment; filename="backup_rentalman_{datetime.datetime.now()}.xlsx"',
    )
    message.attach(part)

    # Connect to the SMTP server and send the email
    try:
        server = smtplib.SMTP(smtp_server, smtp_port)
        server.starttls()
        server.login(sender_email, sender_password)
        server.sendmail(sender_email, receiver_email, message.as_string())
        logger.info("Email sent successfully!")
    except Exception as e:
        print(f"Error occurred: {e}")
        logger.error(f"Error occurred: {e}")
    finally:
        logger.info("Closing SMTP server")
        server.quit()


def send_email(data=None):
    # Define email parameters
    smtp_server = "smtp.gmail.com"
    smtp_port = 587  # For SSL: 465, For TLS: 587
    sender_email = "gpamfilis@gmail.com"
    sender_password = "gpdawxoepemsxbnz"
    receiver_email = "gpamfilis@gmail.com"
    subject = "Subject Here"

    # Initialize the message
    message = MIMEMultipart()
    message["From"] = sender_email
    message["To"] = receiver_email
    message["Subject"] = subject

    # Body text
    body_text = "Hello, this is a test email!"
    message.attach(MIMEText(body_text, "plain"))

    # Connect and send the email
    try:
        server = smtplib.SMTP(smtp_server, smtp_port)
        server.starttls()  # For secure connection
        server.login(sender_email, sender_password)
        server.sendmail(sender_email, receiver_email, message.as_string())
        print("Email sent successfully!")
    except Exception as e:
        print(f"Error occurred: {e}")
    finally:
        server.quit()


def send_email_error(
    subject="Failed Message Queue Notification",
    message_data="The message data that failed.",
):
    """
    Send an email notification when a message fails to publish to the queue.

    Parameters:
    - receiver_email (str): The email address of the recipient.
    - message_data (dict or str): The data of the message that failed to be published.
    """
    try:
        # Define email parameters
        smtp_server = "smtp.gmail.com"
        smtp_port = 587  # For SSL: 465, For TLS: 587
        sender_email = "gpamfilis@gmail.com"
        sender_password = "gpdawxoepemsxbnz"
        receiver_email = "gpamfilis@gmail.com"
        subject = subject

        # Initialize the message
        message = MIMEMultipart()
        message["From"] = sender_email
        message["To"] = receiver_email
        message["Subject"] = subject

        # Body text
        body_text = f"Hello, this is an automated notification that a message failed to publish to the queue. Here is the data: {message_data}"
        message.attach(MIMEText(body_text, "plain"))

        # Connect and send the email
        server = smtplib.SMTP(smtp_server, smtp_port)
        server.starttls()  # For secure connection
        server.login(sender_email, sender_password)
        server.sendmail(sender_email, receiver_email, message.as_string())
        logger.info("Email sent successfully to notify about the failed queue message.")
    except Exception as e:
        logger.error(f"Error occurred when sending the failed queue message email: {e}")
    finally:
        # Ensures the server is closed even if an error occurs
        try:
            server.quit()
        except UnboundLocalError:
            # Handle the case where the server instance was never created due to an earlier exception
            logger.error(
                "Failed to quit the SMTP server because the connection was not established."
            )
