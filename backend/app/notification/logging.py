import json
import logging
import sys
import threading
import time

import redis
import requests


class HTTPHandler(logging.Handler):
    def __init__(self, endpoint):
        super().__init__()
        self.endpoint = endpoint

    def emit(self, record):
        log_entry = self.format(record)
        thread = threading.Thread(target=self.send_log, args=(log_entry,))
        thread.start()

    def send_log(self, log_entry):
        try:
            response = requests.post(
                self.endpoint,
                data=json.dumps({"message": log_entry}),
                headers={"Content-Type": "application/json"},
            )
            response.raise_for_status()
        except requests.RequestException as e:
            sys.stderr.write(f"Failed to log to server: {e}\n")


# class RedisHandler(logging.Handler):
#     def __init__(self, redis_conn_string, list_name):
#         super().__init__()
#         self.redis_conn_string = redis_conn_string
#         self.list_name = list_name
#         self.redis_client = redis.from_url(redis_conn_string)

#     def emit(self, record):
#         log_entry = self.format(record)
#         thread = threading.Thread(target=self.send_log, args=(log_entry,))
#         thread.start()

#     def send_log(self, log_entry):
#         try:
#             self.redis_client.rpush(self.list_name, json.dumps({"message": log_entry}))
#         except redis.RedisError as e:
#             sys.stderr.write(f"Failed to log to Redis: {e}\n")


class RedisHandler(logging.Handler):
    def __init__(self, redis_conn_string, list_name):
        super().__init__()
        self.redis_conn_string = redis_conn_string
        self.list_name = list_name
        self.redis_client = redis.from_url(redis_conn_string)

    def emit(self, record):
        # Format the message using the handler's formatter
        if self.formatter:
            formatted_message = self.formatter.format(record)
        else:
            formatted_message = record.getMessage()

        # Manually format the time if a formatter with a date format is set
        if self.formatter and hasattr(self.formatter, "datefmt"):
            formatted_time = time.strftime(
                self.formatter.datefmt, time.localtime(record.created)
            )
        else:
            formatted_time = time.strftime(
                "%Y-%m-%d %H:%M:%S", time.localtime(record.created)
            )

        log_data = {
            "time": formatted_time,
            "severity": record.levelname,
            "name": record.name,
            "message": formatted_message,
        }
        thread = threading.Thread(target=self.send_log, args=(log_data,))
        thread.start()

    def send_log(self, log_data):
        try:
            self.redis_client.rpush(self.list_name, json.dumps(log_data))
        except redis.RedisError as e:
            sys.stderr.write(f"Failed to log to Redis: {e}\n")
