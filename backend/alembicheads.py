"""
This script lists all the revisions in your alembic.ini file.
"""
from alembic.config import Config
from alembic.script import ScriptDirectory


def list_revisions(alembic_cfg_path):
    alembic_cfg = Config(alembic_cfg_path)
    script = ScriptDirectory.from_config(alembic_cfg)

    base = script.get_base()
    head = script.get_current_head()

    print("Revisions (from base to head):")
    for revision in script.iterate_revisions(head, base):
        print(f"Revision ID: {revision.revision} - {revision.doc}")


# Replace with your alembic.ini file path
list_revisions("./alembic.ini")
