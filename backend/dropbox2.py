import dropbox


def get_access_token(app_key, app_secret):
    flow = dropbox.DropboxOAuth2FlowNoRedirect(app_key, app_secret)

    # Generate the URL for Dropbox to authenticate
    authorize_url = flow.start()
    print("1. Go to: " + authorize_url)
    print("2. Click 'Allow' (you might have to log in first)")
    print("3. Copy the authorization code.")

    auth_code = input("Enter the authorization code here: ")

    # This will fail if the user enters an invalid authorization code
    oauth_result = flow.finish(auth_code)
    return oauth_result.access_token


def upload_file(access_token, local_file_path, dropbox_file_path):
    dbx = dropbox.Dropbox(access_token)

    with open(local_file_path, "rb") as f:
        dbx.files_upload(
            f.read(), dropbox_file_path, mode=dropbox.files.WriteMode.overwrite
        )

    print(f"File uploaded to {dropbox_file_path}")


if __name__ == "__main__":
    # Use your own app_key and app_secret
    APP_KEY = "6m7qqxjdy4fm0rn"
    APP_SECRET = "dn5wh3f8i0u1l86"

    # Obtain an access token
    access_token = get_access_token(APP_KEY, APP_SECRET)
    print(access_token)
    # # Local file path and Dropbox file path
    # local_file_path = 'example.txt'
    # dropbox_file_path = '/example.txt'

    # Upload the file
    # upload_file(access_token, local_file_path, dropbox_file_path)
