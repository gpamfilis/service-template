import os

import psycopg2
from dotenv import load_dotenv

load_dotenv(override=True)

SQLALCHEMY_DATABASE_URI = os.getenv("SQLALCHEMY_DATABASE_URI", "local")


def list_connections():
    # query = "SELECT * FROM pg_stat_activity" # This query will return all the columns
    query = "SELECT pid, datname, usename, application_name, state, query FROM pg_stat_activity;"

    try:
        with psycopg2.connect(SQLALCHEMY_DATABASE_URI) as conn:
            with conn.cursor() as cur:
                cur.execute(query)
                connections = cur.fetchall()
                print("Nuber of connections:", len(connections))
                for conn in connections:
                    print(conn)
    except Exception as e:
        print(f"Error: {e}")


list_connections()
