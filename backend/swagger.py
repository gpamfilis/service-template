import json

from apispec import APISpec
from apispec.ext.marshmallow import MarshmallowPlugin
from apispec_webframeworks.flask import FlaskPlugin
from dotenv import load_dotenv
from flask_script import Manager

from app import create_app
from app.api import booking, car, payment, staff
from app.bizlogic.schemas import (
    CreatePaymentSchema,
    EditPaymentSchema,
    GetPaymentSchema,
)
from app.bizlogic.schemas.booking import (
    BookingIDParameter,
    BookingPriceSchema,
    BookingSchema,
    GetBookingPriceSchema,
    InsuranceBreakDownSchema,
)
from app.bizlogic.schemas.car import (
    CarIDParameter,
    CreateCarSchema,
    EditCarSchema,
    GetCarSchema,
)
from app.bizlogic.schemas.shared import PaymentIDParameter
from app.bizlogic.schemas.staff import (
    CreateStaffSchema,
    EditStaffSchema,
    GetStaffSchema,
    StaffIDParameter,
)

load_dotenv()

manager = Manager(create_app())


@manager.command
def apijson():
    app = create_app()
    spec = APISpec(
        title="rentalman",
        version="0.1.0",
        openapi_version="3.0.2",
        info=dict(
            description="Here lives the API Documentation for the rentalman app."
        ),
        plugins=[FlaskPlugin(), MarshmallowPlugin()],
    )

    # api_key_scheme = {"type": "apiKey", "in": "header", "name": "X-API-Key"}
    # jwt_scheme = {"type": "http", "scheme": "bearer", "bearerFormat": "JWT"}

    with app.test_request_context():
        # CAR
        spec.path(view=car.get_car)
        spec.path(view=car.list_cars)
        spec.path(view=car.create_car)
        spec.path(view=car.edit_car)
        spec.path(view=car.delete_car)

        spec.components.schema("CreateCarSchema", schema=CreateCarSchema)
        spec.components.schema("CarIDParameter", schema=CarIDParameter)
        spec.components.schema("EditCarSchema", schema=EditCarSchema)
        spec.components.schema("GetCarSchema", schema=GetCarSchema)

        # STAFF
        spec.path(view=staff.get_staff)
        spec.path(view=staff.list_staff)
        spec.path(view=staff.create_staff)
        spec.path(view=staff.edit_staff)
        spec.path(view=staff.delete_staff)

        spec.components.schema("StaffIDParameter", schema=StaffIDParameter)
        spec.components.schema("CreateStaffSchema", schema=CreateStaffSchema)
        spec.components.schema("EditStaffSchema", schema=EditStaffSchema)
        spec.components.schema("GetStaffSchema", schema=GetStaffSchema)

        # PAYMENT
        spec.path(view=payment.create_payment)
        spec.path(view=payment.list_payments)
        spec.path(view=payment.edit_payment)
        spec.path(view=payment.delete_payment)

        spec.components.schema("GetPaymentSchema", schema=GetPaymentSchema)
        spec.components.schema("EditPaymentSchema", schema=EditPaymentSchema)
        spec.components.schema("CreatePaymentSchema", schema=CreatePaymentSchema)
        spec.components.schema("PaymentIDParameter", schema=PaymentIDParameter)

        # BOOKING
        spec.path(view=booking.list_bookings)
        spec.path(view=booking.get_booking)
        spec.path(view=booking.update_basic_info)
        spec.path(view=booking.update_car)
        spec.path(view=booking.update_booking_note)
        spec.path(view=booking.get_booking_price)
        spec.components.schema("BookingIDParameter", schema=BookingIDParameter)
        spec.components.schema("BookingSchema", schema=BookingSchema)
        spec.components.schema("BookingPriceSchema", schema=BookingPriceSchema)
        spec.components.schema(
            "InsuranceBreakDownSchema", schema=InsuranceBreakDownSchema
        )
        spec.components.schema("GetBookingPriceSchema", schema=GetBookingPriceSchema)

        output = spec.to_dict()
        breakpoint()

        # TODO: add custom tag ordering
        with open("./app/swagger.json", "w") as out:
            json.dump(output, out)


if __name__ == "__main__":
    manager.run()
