import os

from dotenv import load_dotenv

basedir = os.path.abspath(os.path.dirname(__file__))

# TODO prepare the production-specific dotenv file instead of having this check
if "FLASK_ENV" not in os.environ:
    load_dotenv()


class Config:
    def __init__(self):
        pass

    # UPLOAD_EXTENSIONS = [".txt", ".csv", ".xls", ".xlsx"]
    # UPLOAD_PATH = "uploads"
    SECRET_KEY = os.getenv("SECRET_KEY")
    CORS_ALLOW_ORIGIN = "*"
    CORS_ALLOW_METHODS = "OPTIONS, GET, POST, PUT, DELETE"
    CORS_ALLOW_HEADERS = "Content-Type, Accept, Authorization, access_token"
    CORS_EXPOSE_HEADERS = "access_token"

    @staticmethod
    def init_app(app):
        pass


class DevelopmentConfig(Config):
    INTERNAL_URL = "127.0.0.1:5000"

    DEBUG = True
    # SQLALCHEMY_DATABASE_URI = "sqlite:///" + os.path.join(basedir, "test.db")
    SQLALCHEMY_DATABASE_URI = os.getenv("SQLALCHEMY_DATABASE_URI")
    # SQLALCHEMY_DATABASE_URI = 'mysql://root:pass@localhost/dbname'
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class PythonAnywhereConfig(Config):
    INTERNAL_URL = "www.example.com"

    SQLALCHEMY_TRACK_MODIFICATIONS = False

    SQLALCHEMY_DATABASE_URI = os.getenv("SQLALCHEMY_DATABASE_URI_PYTHONANYWHERE")


config = {
    "development": DevelopmentConfig,
    "pythonanywhere": PythonAnywhereConfig,
}
