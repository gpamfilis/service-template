import os

import psycopg2
from dotenv import load_dotenv

load_dotenv(override=True)

SQLALCHEMY_DATABASE_URI = os.getenv("SQLALCHEMY_DATABASE_URI", "local")


def list_and_close_idle_connections():
    idle_query = "SELECT pid FROM pg_stat_activity WHERE state = 'idle in transaction';"
    # idle_query = "SELECT pid FROM pg_stat_activity WHERE state = 'idle';"

    # idle_query = """
    # SELECT pid, datname, usename, application_name, state, query
    # FROM pg_stat_activity
    # WHERE state = 'idle in transaction' OR (datname IS NULL AND usename IS NULL AND application_name = '' AND state IS NULL AND query = '');
    # """
    terminate_query = "SELECT pg_terminate_backend(%s);"

    try:
        with psycopg2.connect(SQLALCHEMY_DATABASE_URI) as conn:
            with conn.cursor() as cur:
                # List and print idle connections
                cur.execute(idle_query)
                connections = cur.fetchall()
                for conn_info in connections:
                    pid = conn_info[0]
                    print(f"Connection info: {conn_info}")
                    try:
                        if pid:  # Check if PID is not None
                            print(f"Terminating connection with PID: {pid}")
                            # Terminate the connection
                            cur.execute(terminate_query, (pid,))
                    except Exception as e:
                        print(f"Error: {e}")

    except Exception as e:
        print(f"Error: {e}")


list_and_close_idle_connections()
