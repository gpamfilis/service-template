# #!/bin/bash
# # Generates changelog day by day
# echo "CHANGELOG"
# echo ----------------------
# git log --no-merges --format="%cd" --date=short | sort -u -r | while read DATE ; do
#     echo
#     echo [$DATE]
#     GIT_PAGER=cat git log --no-merges --format=" * %s" --since="$DATE 00:00:00" --until="$DATE 24:00:00"
# done
# !/bin/bash
# Generates changelog day by day
# echo "CHANGELOG"
# echo ----------------------
# git log --no-merges --format="%cd" --date=short | sort -u -r | while read -r DATE ; do
#     echo
#     echo "[$DATE]"
#     GIT_PAGER=cat git log --no-merges --format=" * %s (Contributor: %cn)" --since="$DATE 00:00:00" --until="$DATE 24:00:00"
# done

#!/bin/bash
# Generates changelog day by day
# echo "CHANGELOG"
# echo ----------------------
# git log --no-merges --format="%cd" --date=short | sort -u -r | while read -r DATE ; do
#     echo
#     echo "[$DATE]"
#     GIT_PAGER=cat git log --no-merges --format=" * %s (Author: %an)" --since="$DATE 00:00:00" --until="$DATE 24:00:00"
# done

#!/bin/bash
# Generates changelog day by day
echo "CHANGELOG"
echo ----------------------
git log --no-merges --format="%cd" --date=short | sort -u -r | while read -r DATE ; do
    echo
    echo "[$DATE]"
    GIT_PAGER=cat git log --no-merges --format=" * %s (Author: %an, Commit: %h)" --since="$DATE 00:00:00" --until="$DATE 24:00:00"
done
