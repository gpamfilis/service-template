import React from "react";

import { Grid } from "@mui/material";
import ProjectList from "./ProjectList";
// import { useDoin } from "../ContextProviders/MainContextProvider";
// import PaymentList from "./PaymentList";
// import MyLinePlot from "./PaymentPlot";

const ProjectPage = () => {
  //   const { selectedProject } = useDoin();
  return (
    <Grid
      container
      spacing={1}
      style={{ height: "calc(100vh - 64px - 100px)" }}
    >
      <Grid item xs={2}>
        <ProjectList />
      </Grid>
    </Grid>
  );
};

export default ProjectPage;
