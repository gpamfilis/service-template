/* eslint-disable no-alert, no-console */
import React, { useEffect, useState } from "react";
import axios from "axios";
import {
  List,
  ListItem,
  ListItemText,
  Paper,
  Box,
  Typography,
  Divider,
  IconButton,
  Snackbar,
} from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import EditIcon from "@mui/icons-material/Edit";
import { useDoin } from "../ContextProviders/MainContextProvider";
import ProjectManager from "../managers/ProjectManager";
import ProjectAddDialog from "./ProjectAddDialog";
import ProjectEditModal from "./ProjectEditDialog";
import PropTypes from "prop-types";
const BACKEND_DOMAIN = process.env.REACT_APP_BACKEND_DOMAIN;

const highlightedStyle = {
  backgroundColor: "#f0f0f0", // Example highlight color, change as needed
};

const ProjectList = ({ show = null, onClickProjectItem = () => {} }) => {
  const { setSelectedProject } = useDoin();
  const { user_token } = useDoin();
  const { setProjects } = useDoin();
  // eslint-disable-next-line no-unused-vars
  const [selectedItemId, setSelectedItemId] = useState(null);
  const [toastMessage, setToastMessage] = useState("");
  const [toastOpen, setToastOpen] = useState(false);

  const {
    // Search
    editProjectModalOpen,
    addProjectModalOpen,
    setAddProjectModalOpen,
    setEditProjectModalOpen,

    // Project
    projects,
    selectedProject,
    setSelectedProjectId,
  } = useDoin();

  const contextValues = useDoin(); // Getting context values
  const projectManager = new ProjectManager(contextValues);

  const fetchProjects = async () => {
    try {
      const response = await axios.get(
        `${BACKEND_DOMAIN}/api/v1/project?token=${user_token}`
      );
      setProjects(response.data);
    } catch (error) {
      console.error("Error fetching projects:", error);
    }
  };

  useEffect(() => {
    fetchProjects();
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  // eslint-disable-next-line no-unused-vars
  const handleItemClick = (project) => {
    setSelectedProject(project);
    setSelectedItemId(project.id);
  };

  // eslint-disable-next-line no-unused-vars
  const handleProjectAddClick = (group) => {
    setAddProjectModalOpen(true);
  };
  const handleProjectEditClick = (project) => {
    setEditProjectModalOpen(true);
    setSelectedProjectId(project.id);
    setSelectedProject(project);
  };

  const handleListProjectItemClick = async (project) => {
    setSelectedProjectId(project.id);
    setSelectedProject(project);
    onClickProjectItem(project);
    // groupManager.retrive_groups_by_project_manual_projectID(project.id);
    // setSearchTerm(project.name);
    // groupManager.get_nodes_by_project_id(project.id);
  };
  const paperStyle = {
    padding: 20,
    display: "flex",
    flexDirection: "column",
    overflow: "auto",
    height: "calc(100vh - 160px)",
  };

  return (
    <Paper style={paperStyle}>
      <Typography variant="h5" style={{ paddingBottom: "20px" }}>
        Projects
      </Typography>

      <Typography variant="body1" style={{ paddingBottom: "20px" }}>
        Select a project to view its payments.
      </Typography>

      <Divider style={{ borderWidth: 2 }} />
      <Box display="flex" alignItems="center" sx={{ paddingLeft: "10px" }}>
        <Typography variant="h6" component="p">
          Projects
        </Typography>
        <IconButton onClick={handleProjectAddClick}>
          <AddIcon />
        </IconButton>
      </Box>

      <Box sx={{ overflow: "auto" }}>
        <List>
          {projects.map(
            (project) =>
              (show === null || (show !== null && project[show])) && (
                <ListItem
                  button
                  key={project.id}
                  onClick={() => handleListProjectItemClick(project)}
                  style={
                    selectedProject && selectedProject.id === project.id
                      ? highlightedStyle
                      : null
                  }
                >
                  <ListItemText
                    primary={project.name}
                    secondary={project.description}
                  />
                  <IconButton onClick={() => handleProjectEditClick(project)}>
                    <EditIcon />
                  </IconButton>
                </ListItem>
              )
          )}
        </List>
      </Box>

      <ProjectAddDialog
        open={addProjectModalOpen}
        handleClose={() => setAddProjectModalOpen(false)}
        handleSubmit={projectManager.handleProjectSubmit}
        title="Add a New Project"
      />

      {selectedProject && (
        <ProjectEditModal
          open={editProjectModalOpen}
          handleClose={() => setEditProjectModalOpen(false)}
          handleSubmit={projectManager.handleProjectEditSubmit}
          handleDelete={() =>
            projectManager.handleDeleteProject({
              setToastMessage,
              setToastOpen,
            })
          }
          initialTask={selectedProject}
          title="Edit Project"
        />
      )}

      <Snackbar
        open={toastOpen}
        autoHideDuration={6000}
        onClose={() => setToastOpen(false)}
        message={toastMessage}
      />
    </Paper>
  );
};

ProjectList.propTypes = {
  show: PropTypes.bool,
  onClickProjectItem: PropTypes.func,
};

export default ProjectList;
