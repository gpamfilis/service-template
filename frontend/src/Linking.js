import Button from "@mui/material/Button";
import WhatsAppIcon from "@mui/icons-material/WhatsApp";
import PhoneIcon from "@mui/icons-material/Phone";
import EmailIcon from "@mui/icons-material/Email";

const CallButton = ({ phoneNumber }) => {
  const handleClick = () => {
    window.open(`tel:${phoneNumber}`, "_self");
  };

  return (
    <Button
      variant="contained"
      color="primary"
      startIcon={<PhoneIcon />}
      onClick={handleClick}
    >
      Call
    </Button>
  );
};
const WhatsAppButton = ({ phoneNumber }) => {
  const handleClick = () => {
    window.open(`https://wa.me/${phoneNumber}`, "_blank");
  };

  return (
    <Button
      variant="contained"
      color="primary"
      startIcon={<WhatsAppIcon />}
      onClick={handleClick}
    >
      WhatsApp
    </Button>
  );
};

const EmailButton2 = ({ email }) => {
  const handleClick = () => {
    window.open(`mailto:${email}`, "_self");
  };

  return (
    <Button
      variant="contained"
      color="primary"
      startIcon={<EmailIcon />}
      onClick={handleClick}
    >
      Email
    </Button>
  );
};

const EmailButton = ({ email }) => {
  const handleClick = () => {
    window.open(
      `https://mail.google.com/mail/u/0/?view=cm&fs=1&to=${email}&tf=1`,
      "_blank"
    );
  };

  return (
    <Button
      variant="contained"
      color="primary"
      startIcon={<EmailIcon />}
      onClick={handleClick}
    >
      Email
    </Button>
  );
};

export { CallButton, WhatsAppButton, EmailButton, EmailButton2 };
