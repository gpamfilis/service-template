import React from "react";
import { useNavigate, useLocation } from "react-router-dom";

import { AppBar, Box, Toolbar, Button } from "@mui/material";
import { useDoin } from "./ContextProviders/MainContextProvider";
import LogoutIcon from "@mui/icons-material/Logout";
import PropTypes from "prop-types";

// Inside your component
const Header = () => {
  const navigate = useNavigate();
  const location = useLocation();

  const { update_user_token, setUserIsAuthenticated, user_is_authenticated } =
    useDoin(); // Use the context

  const contextValues = useDoin();

  const handleLogout = () => {
    update_user_token(null); // Update token to null
    setUserIsAuthenticated(false); // Update authentication to false

    navigate("/login"); // Redirect to login
  };

  return (
    <AppBar position="static">
      <Toolbar sx={(theme) => ({ ...theme.header })}>
        {user_is_authenticated &&
          contextValues.basicPermissions.can_view_projects && (
            <Button
              color="inherit"
              onClick={() => {
                // contextValues.setSelectedProject(null);
                // contextValues.setSelectedGroup(null);

                navigate("/projects");
              }}
              sx={{
                opacity: location.pathname === "/projects" ? 1 : 0.5,
              }}
            >
              Projects
            </Button>
          )}

        <Box display="flex" justifyContent="flex-end" width="100%">
          {user_is_authenticated && (
            <Button
              color="inherit"
              onClick={handleLogout}
              sx={{
                display: "flex",
                flexDirection: "row",
                alignItems: "center",
              }} // Adjust the styling here
              endIcon={<LogoutIcon />} // Add the icon to the end of the button
            >
              Logout
            </Button>
          )}
          {location.pathname === "/login" && (
            <Button color="inherit" onClick={() => navigate("/register")}>
              Register
            </Button>
          )}
          {location.pathname === "/register" && (
            <Button color="inherit" onClick={() => navigate("/login")}>
              Login
            </Button>
          )}
        </Box>
      </Toolbar>
    </AppBar>
  );
};

Header.propTypes = {
  toggleDrawer: PropTypes.func,
};

export default Header;
