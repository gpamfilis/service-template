import React from "react";
import {
  FormControl,
  FormHelperText,
  InputLabel,
  MenuItem,
  Select,
} from "@mui/material";

import PropTypes from "prop-types";

const DropDownGeneralFormik = ({
  items,
  values,
  field_key,
  setFieldValue,
  label,
  name_key,
  touched,
  errors,
  onChange = () => {},
}) => {
  return (
    <FormControl
      variant="standard"
      fullWidth
      style={{ marginBottom: "16px", marginTop: "16px" }}
      error={Boolean(touched[field_key] && errors[field_key])}
    >
      <InputLabel shrink={values[field_key] != null}>{label}</InputLabel>
      <Select
        value={values[field_key]}
        onChange={(e) => {
          onChange(e.target.value);
          setFieldValue(field_key, e.target.value);
        }}
      >
        {items.map((member) => (
          <MenuItem key={member.id} value={member.id}>
            {member[name_key]}
          </MenuItem>
        ))}
      </Select>
      {touched[field_key] && errors[field_key] && (
        <FormHelperText>{errors[field_key]}</FormHelperText>
      )}
    </FormControl>
  );
};

DropDownGeneralFormik.propTypes = {
  items: PropTypes.array.isRequired,
  values: PropTypes.object.isRequired,
  field_key: PropTypes.string.isRequired,
  setFieldValue: PropTypes.func.isRequired,
  label: PropTypes.string.isRequired,
  name_key: PropTypes.string.isRequired,
  touched: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired,
  onChange: PropTypes.func,
};

export default DropDownGeneralFormik;
