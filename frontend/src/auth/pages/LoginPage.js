import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { useFormik } from "formik";
import * as Yup from "yup";
import { useDoin } from "../../ContextProviders/MainContextProvider";
import {
  TextField,
  Button,
  Grid,
  Paper,
  Typography,
  Container,
  Snackbar,
  Tabs,
  Tab,
} from "@mui/material";
import AuthManager from "../../managers/AuthManager";

const LoginPage = () => {
  const [snackbarOpen, setSnackbarOpen] = useState(false);
  const [snackbarMessage, setSnackbarMessage] = useState("");
  const [selectedTab, setSelectedTab] = useState(0);

  const navigate = useNavigate();
  const contextValues = useDoin();
  const authManager = new AuthManager(contextValues, navigate);

  const handleTabChange = (event, newValue) => {
    setSelectedTab(newValue);
  };

  const formik = useFormik({
    initialValues: {
      email: null,
      password: null,
    },
    validationSchema: Yup.object({
      email: Yup.string().required("Required"),
      password: Yup.string().required("Required"),
    }),
    onSubmit: async (values) => {
      if (selectedTab === 0) {
        authManager.login_team(values, setSnackbarMessage, setSnackbarOpen);
      } else {
        authManager.login(values, setSnackbarMessage, setSnackbarOpen);
      }
    },
  });

  return (
    <Container
      component="main"
      maxWidth="xs"
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        height: "100vh",
      }}
    >
      <Paper elevation={6} style={{ padding: "20px", marginTop: "-8vh" }}>
        <Typography component="h1" variant="h5" align="center">
          Login
        </Typography>
        <Tabs
          value={selectedTab}
          onChange={handleTabChange}
          variant="fullWidth"
          indicatorColor="primary"
          textColor="primary"
        >
          <Tab label="Team" />
          <Tab label="Manager" />
        </Tabs>
        <form onSubmit={formik.handleSubmit} style={{ marginTop: "1em" }}>
          <Grid container spacing={2} direction="column">
            <Grid item>
              <TextField
                fullWidth
                label="Email"
                name="email"
                value={formik.values.email}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                error={formik.touched.email && Boolean(formik.errors.email)}
                helperText={formik.touched.email && formik.errors.email}
              />
            </Grid>
            <Grid item>
              <TextField
                fullWidth
                label="Password"
                name="password"
                type="password"
                value={formik.values.password}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                error={
                  formik.touched.password && Boolean(formik.errors.password)
                }
                helperText={formik.touched.password && formik.errors.password}
              />
            </Grid>
            <Grid item>
              <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
              >
                Login
              </Button>
            </Grid>
          </Grid>
        </form>
      </Paper>

      <Snackbar
        open={snackbarOpen}
        autoHideDuration={6000}
        onClose={() => setSnackbarOpen(false)}
        message={snackbarMessage}
        anchorOrigin={{ vertical: "top", horizontal: "center" }}
      />
    </Container>
  );
};

export default LoginPage;
