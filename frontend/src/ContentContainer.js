import React from "react";
import { Routes, Route, Navigate } from "react-router-dom";
import { Container } from "@mui/material";
import ProtectedRoute from "./auth/pages/ProtectedRoute";
import LoginPage from "./auth/pages/LoginPage";
import { useDoin } from "./ContextProviders/MainContextProvider";
import RegisterPage from "./auth/pages/RegisterPage";

import ProjectPage from "./project/ProjectPage";

const ContentContainer = () => {
  const { user_token } = useDoin();
  return (
    <Container
      component="main"
      maxWidth={false}
      disableGutters
      sx={{
        flexGrow: 1,
        height: "100%", // Ensure the container can grow as needed
        overflow: "auto",
      }}
    >
      <Routes>
        <Route path="/login" element={<LoginPage />} />
        <Route path="/register" element={<RegisterPage />} />
        <Route
          path="/projects"
          element={
            <ProtectedRoute>
              <ProjectPage />
            </ProtectedRoute>
          }
        />
        <Route
          path="/"
          element={
            <ProtectedRoute>
              <LoginPage />
            </ProtectedRoute>
          }
        />
        <Route path="/" element={<Navigate to={user_token ? "/" : "/"} />} />
      </Routes>
    </Container>
  );
};

export default ContentContainer;
